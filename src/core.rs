use std::collections::HashMap;
use std::fmt;

#[derive(PartialEq, Eq, Copy, Clone)]
pub enum Row { Top, Mid, Bot }

pub type Col = u8;

pub type Offset = f32;

const INWARD_THRESHOLD: Offset = 0.0;
const OUTWARD_THRESHOLD: Offset = -1.0/2.0;

#[derive(PartialEq, Eq, Copy, Clone)]
pub enum Hand {
    Left = -1, Right = 1
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Copy, Clone)]
pub enum Finger {
    Index, Middle, Ring, Little
}

const FINGERS: [Finger; 4] = [Finger::Index, Finger::Middle, Finger::Ring, Finger::Little];

#[derive(Clone)]
struct HalfKeyboard {
    keys: HashMap<char, (Row, Col)>,
    stagger: [f32; 3],
    home: [(Row, Col); 4]
}

#[derive(Clone)]
pub struct Keyboard {
    left: HalfKeyboard,
    right: HalfKeyboard
}

impl HalfKeyboard {
    fn new(top: String, mid: String, bot: String, home: [(Row, Col); 4], stagger: [f32; 3]) -> Self {
        let mut keys = HashMap::new();
        for (col, key) in top.char_indices() { keys.insert(key, (Row::Top, col as u8)); }
        for (col, key) in mid.char_indices() { keys.insert(key, (Row::Mid, col as u8)); }
        for (col, key) in bot.char_indices() { keys.insert(key, (Row::Bot, col as u8)); }

        HalfKeyboard { keys, stagger, home }
    }
}

impl Keyboard {
    pub fn new<S: Into<String>>(left_top: S, left_mid: S, left_bot: S, left_home: [(Row, Col); 4],
                                right_top: S, right_mid: S, right_bot: S, right_home: [(Row, Col); 4],
                                stagger_left: [f32; 3], stagger_right: [f32; 3]) -> Self {
        Keyboard {
            left: HalfKeyboard::new(left_top.into(), left_mid.into(), left_bot.into(), left_home, stagger_left),
            right: HalfKeyboard::new(right_top.into(), right_mid.into(), right_bot.into(), right_home, stagger_right)
        }
    }

    fn half(&self, side: Hand) -> &HalfKeyboard {
        match side {
            Hand::Left => &self.left,
            _          => &self.right
        }
    }

    fn half_mut(&mut self, side: Hand) -> &mut HalfKeyboard {
        match side {
            Hand::Left => &mut self.left,
            _          => &mut self.right
        }
    }

    pub fn position(&self, c: char) -> (Hand, Row, Col) {
        let (hand, hkb) = if self.left.keys.contains_key(&c) { (Hand::Left, &self.left) }
             else if self.right.keys.contains_key(&c) { (Hand::Right, &self.right) }
             else { panic!("Character not found: `{}'", c); };

        let (row, col) = hkb.keys[&c];
        (hand, row, col)
    }

    pub fn swap(&mut self, a: char, b: char) {
        let left_len = self.left.keys.len();
        let right_len = self.right.keys.len();
        let (hand0, _, _) = self.position(a);
        let (hand1, _, _) = self.position(b);
        let pos0 = self.half_mut(hand0).keys.remove(&a).unwrap();
        let pos1 = self.half_mut(hand1).keys.remove(&b).unwrap();
        self.half_mut(hand0).keys.insert(a, pos1);
        self.half_mut(hand1).keys.insert(b, pos0);
        assert!(left_len == self.left.keys.len());
        assert!(right_len == self.right.keys.len());
    }

    pub fn real_position(&self, c: char) -> Offset {
        let (hand, row, col) = self.position(c);
        let hkb = self.half(hand);
        col as f32 + hkb.stagger[row as usize]
    }

    pub fn stagger(&self, hand: Hand, row: Row) -> Offset {
        self.half(hand).stagger[row as usize]
    }
}

impl fmt::Display for Keyboard {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut ltop: Vec<_> = self.left.keys.iter().filter(|(_, (row, _))| *row == Row::Top).map(|(k, (_, col))| (k, col)).collect();
        let mut lmid: Vec<_> = self.left.keys.iter().filter(|(_, (row, _))| *row == Row::Mid).map(|(k, (_, col))| (k, col)).collect();
        let mut lbot: Vec<_> = self.left.keys.iter().filter(|(_, (row, _))| *row == Row::Bot).map(|(k, (_, col))| (k, col)).collect();
        let mut rtop: Vec<_> = self.right.keys.iter().filter(|(_, (row, _))| *row == Row::Top).map(|(k, (_, col))| (k, col)).collect();
        let mut rmid: Vec<_> = self.right.keys.iter().filter(|(_, (row, _))| *row == Row::Mid).map(|(k, (_, col))| (k, col)).collect();
        let mut rbot: Vec<_> = self.right.keys.iter().filter(|(_, (row, _))| *row == Row::Bot).map(|(k, (_, col))| (k, col)).collect();

        ltop.sort_unstable_by_key(|x| x.1);
        lmid.sort_unstable_by_key(|x| x.1);
        lbot.sort_unstable_by_key(|x| x.1);
        rtop.sort_unstable_by_key(|x| x.1);
        rmid.sort_unstable_by_key(|x| x.1);
        rbot.sort_unstable_by_key(|x| x.1);
        ltop.reverse();
        lmid.reverse();
        lbot.reverse();

        let mut ltop: Vec<_> = ltop.into_iter().map(|(k, _)| k).collect();
        let mut lmid: Vec<_> = lmid.into_iter().map(|(k, _)| k).collect();
        let mut lbot: Vec<_> = lbot.into_iter().map(|(k, _)| k).collect();
        let rtop: Vec<_> = rtop.into_iter().map(|(k, _)| k).collect();
        let rmid: Vec<_> = rmid.into_iter().map(|(k, _)| k).collect();
        let rbot: Vec<_> = rbot.into_iter().map(|(k, _)| k).collect();

        let left_longest = ltop.len().max(lmid.len()).max(lbot.len());
        for _ in 0..left_longest - ltop.len() { ltop.push(&' '); }
        for _ in 0..left_longest - lmid.len() { lmid.push(&' '); }
        for _ in 0..left_longest - lbot.len() { lbot.push(&' '); }

        let mut top = String::with_capacity((ltop.len() + rtop.len()) * 2 + 1);
        for c in ltop { top.extend([*c, ' '].into_iter()); }
        for c in rtop { top.extend([' ', *c].into_iter()); }

        let mut mid = String::with_capacity((lmid.len() + rmid.len()) * 2 + 1);
        for c in lmid { mid.extend([*c, ' '].into_iter()); }
        for c in rmid { mid.extend([' ', *c].into_iter()); }

        let mut bot = String::with_capacity((lbot.len() + rbot.len()) * 2 + 1);
        for c in lbot { bot.extend([*c, ' '].into_iter()); }
        for c in rbot { bot.extend([' ', *c].into_iter()); }

        write!(f, "{}\n{}\n{}", top, mid, bot)
    }
}

struct Keypress {
    hand: Hand,
    finger: Finger,
    key: char
}

pub const STAGGERED_STAGGER: [f32; 3] = [0.0, 4.0/16.0, 13.0/16.0];
pub const ORTHOLINEAR_STAGGER: [f32; 3] = [0.0, 0.0, 0.0];

// The cost of a keypress is the given by (finger-cost + placement-cost) × motion-cost. The finger-cost is uniquely
// determined by the finger used. The placement-cost is uniquely determined by the finger used, as well as the key
// pressed, allowing for hypotheses like that employed by the Workman layout that the index finger prefers to curl
// inwards rather than stretch outwards, while the other fingers prefer to stretch outwards. The motion-cost is uniquely
// determined by the finger used and the key pressed, as well as the previous finger used and key pressed. This allows
// us to avoid using the same finger twice in a row for different keys.
//
// The finger used is chosen so as to minimize the total cost of the keypress; for example, when typing the first two
// letters of the word "human" on a QWERTY keyboard, it is common for the middle finger, rather than the index finger,
// is used to type the letter "u", in order to avoid incurring the motion-cost penalty of using the index finger twice
// in a row. The placement-cost should be chosen so that each finger can travel far enough to provide such
// substitutions, and essentially ∞ beyond that range.  As an example of an upper bound, I allow my middle finger to
// stray as far as the letter "y" when typing "hymen" on a QWERTY keyboard, using fingering index-middle-index for
// "hym". More extremely, in a phrase like "why is", I use fingering on "hy i" index-middle-ring.

// Keyboards looking to optimize for handedness, á la left/right-handed Dvorak
fn finger_cost(_hand: Hand, finger: Finger, _kb: &Keyboard) -> f32 {
    match finger {
        Finger::Index => 1.0,
        Finger::Middle => 1.0,
        Finger::Ring => 1.5,
        Finger::Little => 2.0
    }
}

// Any independent subcalculations which depend only on hand and finger should be in finger_cost
fn placement_cost(press: &Keypress, kb: &Keyboard) -> f32 {
    let (hand, row, col) = kb.position(press.key);

    if hand != press.hand {
        return std::f32::INFINITY;
    }

    let row_cost = if press.finger == Finger::Index {
        match row {
            Row::Top => 2.0,
            Row::Mid => 0.0,
            Row::Bot => 1.0
        }
    } else {
        match row {
            Row::Top => 1.0,
            Row::Mid => 0.0,
            Row::Bot => 2.0
        }
    };
    let offset = kb.real_position(press.key);
    let home_offset = kb.stagger(hand, row) + col as f32;
    // If going inwards from the home position, col_cost = col distance. If going outwards, twice that
    let inward_motion = offset - home_offset * press.hand as i32 as f32;
    let col_cost = if inward_motion > 0.0 { inward_motion }
                                   else { -inward_motion * 2.0 };
    row_cost + col_cost
}

fn motion_cost(press: &Keypress, prev_press: &Keypress, kb: &Keyboard) -> f32 {
    if press.hand != prev_press.hand {
        return 1.0;
    }

    if press.finger == prev_press.finger && press.key == prev_press.key {
        if press.key == prev_press.key {
            return 0.0;
        } else {
            return 3.0;
        }
    }

    // A finger cannot pass through another finger, e.g. the index finger must always be to the
    // left of the middle finger. There is some small tolerance, e.g. 'ju' is pressable on a
    // staggered QWERTY keyboard because U is only slightly further left than J.
    let inward_motion = press.hand as i32 as f32 * (kb.real_position(press.key) - kb.real_position(prev_press.key));
    if (press.finger < prev_press.finger && inward_motion > INWARD_THRESHOLD) ||
       (press.finger > prev_press.finger && -inward_motion > OUTWARD_THRESHOLD ) {
        return std::f32::INFINITY;
    }

    return 1.0;
}

fn total_cost(press: &Keypress, prev_press: &Keypress, kb: &Keyboard) -> f32 {
    motion_cost(&press, &prev_press, &kb) *
        (finger_cost(press.hand, press.finger, &kb) + placement_cost(&press, &kb))
}

fn total_cost_initial(press: &Keypress, kb: &Keyboard) -> f32 {
    finger_cost(press.hand, press.finger, &kb) + placement_cost(&press, &kb)
}

fn choose_finger(key: char, prev_press: &Keypress, kb: &Keyboard) -> (Keypress, f32) {
    let (hand, _, _) = kb.position(key);
    let candidates = FINGERS.iter().map(|finger| Keypress { hand, finger: *finger, key });
    let dummy_keypress = Keypress { hand: Hand::Left, finger: Finger::Index, key: ' ' };
    let initial = (dummy_keypress, std::f32::INFINITY);
    candidates.fold(initial, |(best_finger, best_cost), new_finger| {
        let new_cost = total_cost(&new_finger, &prev_press, &kb);
        if new_cost < best_cost { (new_finger, new_cost) }
                           else { (best_finger, best_cost) }
    })
}

fn choose_finger_initial(key: char, kb: &Keyboard) -> (Keypress, f32) {
    let (hand, _, _) = kb.position(key);
    let candidates = FINGERS.iter().map(|finger| Keypress { hand, finger: *finger, key });
    let dummy_keypress = Keypress { hand: Hand::Left, finger: Finger::Index, key: ' ' };
    let initial = (dummy_keypress, std::f32::INFINITY);
    candidates.fold(initial, |(best_finger, best_cost), new_finger| {
        let new_cost = total_cost_initial(&new_finger, &kb);
        if new_cost < best_cost { (new_finger, new_cost) }
                           else { (best_finger, best_cost) }
    })
}

pub fn word_cost<I>(mut word: I, kb: &Keyboard) -> f32 where I: Iterator<Item = char> {
    let mut cost = 0.0;
    let mut prev_press;
    if let Some(key) = word.next() {
        let (press, key_cost) = choose_finger_initial(key, &kb);
        prev_press = press;
        cost = key_cost;

        for key in word {
            let (press, key_cost) = choose_finger(key, &prev_press, &kb);
            prev_press = press;
            cost += key_cost;
        }
    }
    assert!(!cost.is_nan());
    assert!(!cost.is_infinite());
    cost
}
