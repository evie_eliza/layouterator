use std::collections::{BTreeSet, HashSet, HashMap};
use rand::prelude::*;
use rand::seq::IteratorRandom;
use rand::seq::SliceRandom;
use std::fs::{self, File, DirEntry};
use std::io::{BufRead, BufReader};
use std::mem::swap;
use std::iter::FromIterator;

use crate::core::*;

#[derive(Clone, PartialEq, Eq)]
pub struct SwapPoint(pub char, pub char);

// The equality relation is too conservative, but this shouldn't actually matter because genomes
// aren't generally compared directly
#[derive(PartialEq, Clone)]
pub struct Genome {
    pub genome: Vec<SwapPoint>
}

#[derive(Clone)]
pub struct Phenotype {
    genome: Genome,
    representation: Keyboard,
    cost: f32
}

pub struct Alphabet {
    alphabet: HashSet<char>,
    conversions: HashMap<char, char>
}

// This is to manage the fact that keys have multiple letters; in the simplest case, q and Q share
// a key, as do ' and ", and [ and {. However, this approach also allows for things like AltGr or
// other modifier keys.
impl Alphabet {
    pub fn new(alphabet: HashSet<char>, conversions_pairs: &[(char, char)]) -> Alphabet {
        let mut conversions = HashMap::new();
        for (from, to) in conversions_pairs {
            assert!(alphabet.contains(to));
            conversions.insert(*from, *to);
        }
        Alphabet { alphabet, conversions }
    }

    pub fn contains(&self, c: char) -> bool {
        if self.alphabet.contains(&c) {
            true
        } else {
            self.conversions.contains_key(&c) && self.alphabet.contains(&self.conversions[&c])
        }
    }

    pub fn convert(&self, chars: &str) -> String {
        let chars = chars.chars().map(|c|
            self.alphabet.get(&c)
                         .or_else(|| self.conversions.get(&c))
                         .map(char::clone)
                         .unwrap_or(c));
        String::from_iter(chars)
    }

    pub fn choose_multiple<R: rand::Rng + ?Sized>(&self, rng: &mut R, amount: usize) -> Vec<char> {
        self.alphabet.iter().cloned().choose_multiple(rng, amount)
    }
}

impl PartialEq for Phenotype {
    fn eq(&self, other: &Phenotype) -> bool {
        self.genome == other.genome
    }
}

impl Eq for Phenotype {}

impl PartialOrd for Phenotype {
    fn partial_cmp(&self, other: &Phenotype) -> Option<std::cmp::Ordering> {
        self.cost.partial_cmp(&other.cost)
    }
}

impl Ord for Phenotype {
    fn cmp(&self, other: &Phenotype) -> std::cmp::Ordering {
        self.partial_cmp(&other).unwrap()
    }
}

// A phenotype that hasn't yet had its cost calculated
struct Fledgling(Phenotype);

impl Fledgling {
    fn new(genome: Genome, representation: Keyboard) -> Fledgling {
        Fledgling(Phenotype { genome, representation, cost: 0.0 })
    }

    fn fledge(self) -> Phenotype {
        self.0
    }
}

// Using a module here to enforce immutability
pub mod eco_parameters {
    use std::path::PathBuf;
    use crate::core::*;

    pub struct EcoParameters {
        base_: Keyboard,
        texts_dir_: PathBuf,
        sample_size_: usize,
        brood_size_: usize,
        mutation_rate_: f32
    }

    impl EcoParameters {
        pub fn base(&self) -> &Keyboard { &self.base_ }
        pub fn texts_dir(&self) -> &PathBuf { &self.texts_dir_ }
        pub fn sample_size(&self) -> usize { self.sample_size_ }
        pub fn brood_size(&self) -> usize { self.brood_size_ }
        pub fn mutation_rate(&self) -> f32 { self.mutation_rate_ }
        pub fn new(base_: Keyboard, texts_dir_: PathBuf, sample_size_: usize, brood_size_: usize, mutation_rate_: f32) -> EcoParameters {
            EcoParameters { base_, texts_dir_, sample_size_, brood_size_, mutation_rate_ }
        }
    }
}
use eco_parameters::*;

pub struct Ecosystem {
    organisms: BTreeSet<Phenotype>,
    alphabet: Alphabet,
    total_cost: f32,
    params: EcoParameters,
}

impl Ecosystem {
    pub fn new(initial_population: Vec<Genome>, alphabet: Alphabet, parameters: EcoParameters) -> Ecosystem {
        let mut ecosystem = Ecosystem {
            organisms: BTreeSet::new(),
            alphabet,
            total_cost:  0.0,
            params: parameters
        };
        let fledglings: Vec<_> = initial_population.into_iter().map(|genome| ecosystem.incubate(genome)).collect();
        let organisms = ecosystem.assign_cost(fledglings);
        ecosystem.total_cost += organisms.iter().map(|Phenotype { cost, .. }| cost).sum::<f32>();
        ecosystem.organisms.extend(organisms);

        ecosystem
    }

    fn incubate(&self, genome: Genome) -> Fledgling {
        let mut kb = self.params.base().clone();
        for SwapPoint(a, b) in genome.genome.iter() {
            kb.swap(*a, *b);
        }
        Fledgling::new(genome, kb)
    }

    fn crossover(&self, mother0: &Genome, mother1: &Genome) -> Genome {
        let mut rng = rand::thread_rng();
        let split0 = match mother0.genome.len() {
            1 => 1,
            len => rng.gen_range(1, len) };
        let split1 = match mother1.genome.len() {
            1 => 1,
            len => rng.gen_range(1, len) };
        let mut child_genome = Vec::with_capacity(split0 + mother1.genome.len() - split1);
        child_genome.extend_from_slice(&mother0.genome[..split0]);
        child_genome.extend_from_slice(&mother1.genome[split1..]);
        Genome { genome: child_genome }
    }

    fn mutate(&self, mut genome: Genome, rate: f32) -> Genome {
        let mut rng = rand::thread_rng();
        for chromosome in genome.genome.iter_mut() {
            if rng.gen::<f32>() < rate {
                let chars = self.alphabet.choose_multiple(&mut rng, 2);
                *chromosome = SwapPoint(chars[0], chars[1]);
            }
        }
        genome
    }

    // Helper function for `assign_cost`. Evaluates word_cost on each word in `line` and adds its
    // value to the organism. A word is defined here as a contiguous sequence of alphabetic characters,
    // which is not a proper subsequence of any other such sequence.
    fn add_costs_line(&self, fledglings: &[Fledgling], line: String) -> Vec<f32> {
        let mut word_start = None;
        let mut costs: Vec<_> = fledglings.iter().map(|_| 0.0).collect();
        let mut num_word_chars = 0;

        let line = self.alphabet.convert(&line);

        for (idx, chr) in line.char_indices() {
            match word_start {
                Some(start) => // If we've reached a word boundary or end of line, calculate word_cost
                    if !self.alphabet.alphabet.contains(&chr) || idx == line.len() - 1 {
                        // If the word boundary is the end of the line, move idx to the EOL rather
                        // than the last word-character
                        let idx = if idx == line.len() - 1 { idx + 1 } else { idx };
                        num_word_chars += idx - start;
                        for (cost, kb) in costs.iter_mut().zip(fledglings.iter().map(|x| &x.0.representation)) {
                            let wc = word_cost(line[start..idx - 1].chars(), kb) / (idx - start) as f32;
                            *cost += wc;
                        }
                        word_start = None;
                    },
                None => if self.alphabet.contains(chr) { word_start = Some(idx); }
            }
        }
         
        if num_word_chars == 0 {
            return costs.into_iter().map(|_| 0.0).collect();
        }

        for cost in costs.iter_mut() {
            *cost /= num_word_chars as f32;
        }

        assert!(costs.iter().all(|x| !x.is_infinite()));
        assert!(costs.iter().all(|x| !x.is_nan()));
        costs
    }

    // We randomly select some texts to test the fledglings' efficiency on, and then read through each
    // one, evaluating the fledglings on them. We take all fledglings at once so that the files only
    // need to be opened once.
    fn assign_cost(&self, mut fledglings: Vec<Fledgling>) -> Vec<Phenotype> {
        // Read in a vector of files, panicking as appropriate
        let texts: Vec<DirEntry> = fs::read_dir(self.params.texts_dir())
            .expect(&format!("Couldn't find directory {}",
                             &self.params.texts_dir().to_string_lossy()))
            .map(|x| x.expect(&format!("Couldn't find file {}",
                              &self.params.texts_dir().to_string_lossy())))
            .collect();
        let mut rng = rand::thread_rng();

        let sample_texts = texts[..].choose_multiple(&mut rng, self.params.sample_size());

        for path in sample_texts {
            let file = File::open(path.path()).expect(&format!("Couldn't open sample {}",
                                                      path.path().to_string_lossy()));
            let file = BufReader::new(file);

            let mut num_lines = 0;
            let mut sample_costs: Vec<_> = fledglings.iter().map(|_| 0.0).collect();

            for line in file.lines() {
                let line = line.expect(&format!("There was a problem reading a line in {}",
                                                path.path().to_string_lossy()));
                let line_costs = self.add_costs_line(&fledglings, line);
                for (sample_cost, line_cost) in sample_costs.iter_mut().zip(line_costs.iter()) {
                    *sample_cost += line_cost;
                }
                num_lines += 1;
            }
            for (fledgling, sample_cost) in fledglings.iter_mut().zip(sample_costs.iter()) {
                fledgling.0.cost += sample_cost / num_lines as f32;
            }

        }

        for fledgling in fledglings.iter_mut() {
            fledgling.0.cost /= self.params.sample_size() as f32;
            assert!(fledgling.0.cost.is_normal());
        }


        // The fledglings are now fully-fledged birbs
        fledglings.into_iter().map(Fledgling::fledge).collect()
    }

    fn select_parents(&self) -> (&Phenotype, &Phenotype) {
        // These declarations look worse than they are, I promise…
        let mut rng = rand::thread_rng();
        let roll0: f32 = rng.gen_range(0.0, &self.total_cost);
        let roll1: f32 = rng.gen_range(0.0, &self.total_cost);
        let mut parent0 = None;
        let mut parent1 = None;
        let mut sum = 0.0;
        // Our fitness function is basically cost × -1. In essence, the BTreeSet ensures organisms are sorted
        // descendingly by fitness, and we choose the last one whose accumulated fitness (fitness + the fitness of all
        // previous ones) is less than or equal to a random number. As the BTreeSet takes care of sorting at insertion,
        // and fitness is only evaluated once, this is not too computationally demanding.
        for organism in self.organisms.iter() {
            if parent0.is_none() && roll0 >= self.total_cost - sum - organism.cost { parent0 = Some(organism); }
            if parent1.is_none() && roll1 >= self.total_cost - sum - organism.cost { parent1 = Some(organism); }
            if parent0.is_some() && parent1.is_some() {
                break;
            }

            sum += organism.cost;
        }
        // It is possible for no parent to be selected because of precision loss; sum may be
        // something like 0.0000000000000004440892098500626
        let last = self.organisms.iter().next_back().unwrap();
        (parent0.unwrap_or(last), parent1.unwrap_or(last))
    }

    fn breed(&mut self) -> Vec<Phenotype> {
        let eggs: Vec<Genome> = (0..self.params.brood_size()).into_iter()
            .map(|_| self.select_parents())
            .map(|(mother0, mother1)| self.crossover(&mother0.genome, &mother1.genome))
            .map(|x| self.mutate(x, self.params.mutation_rate()))
            .collect();
        let fledglings: Vec<Fledgling> = eggs.into_iter()
            .map(|egg| self.incubate(egg))
            .collect();
        let organisms: Vec<Phenotype> = self.assign_cost(fledglings);
        self.total_cost += organisms.iter().map(|Phenotype { cost, .. }| cost).sum::<f32>();
        organisms
    }

    fn discard(&mut self) {
        let mut rng = rand::thread_rng();
        let discards: Vec<_> = self.organisms.iter()
            .cloned()
            .skip(1) // Never discard the current best solution
            .choose_multiple(&mut rng, self.params.brood_size());
        self.total_cost -= discards.iter().map(|Phenotype { cost, .. }| cost).sum::<f32>();
        for discard in discards {
            self.total_cost -= discard.cost;
            self.organisms.remove(&discard);
        }
    }

    pub fn next_generation(&mut self) {
        let chicks = self.breed();
        self.total_cost += chicks.iter().map(|Phenotype { cost, .. }| cost).sum::<f32>();
        self.organisms.extend(chicks);
        self.discard();
    }

    // It may be that something got lucky in which samples it was tested on, or that self.total_cost has drifted away
    // from self.organisms.map(|x| x.cost).sum() via floating point precision loss. Whatever the case, this function
    // recalculates total_cost and all costs.
    pub fn recalculate(&mut self) {
        // Taking ownership of self.organisms
        let mut organisms = BTreeSet::new();
        swap(&mut organisms, &mut self.organisms);
        // So we can more efficiently turn them back into Fledglings
        let mut fledglings: Vec<_> = organisms.into_iter().map(Fledgling).collect();
        for fledgling in fledglings.iter_mut() { fledgling.0.cost = 0.0; }
        let organisms = self.assign_cost(fledglings);
        self.organisms = organisms.into_iter().collect();
        self.total_cost = self.organisms.iter().map(|Phenotype { cost, .. }| cost).sum();
    }

    pub fn best_solution(&self) -> &Keyboard {
        &self.organisms.iter().next().unwrap().representation
    }

    pub fn best_cost(&self) -> f32 {
        self.organisms.iter().next().unwrap().cost
    }
}
