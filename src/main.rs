use std::collections::HashSet;
use std::path::PathBuf;
use rand::prelude::*;

mod core;
mod genetics;

use crate::core::*;
use crate::genetics::*;

fn init() -> Ecosystem {
    let mut conversions = Vec::with_capacity(32);
    for c in String::from("abcdefghijklmnopqrstuvwxyz").chars() {
        conversions.push((c.to_ascii_uppercase(), c));
    }
    conversions.extend([('"', '\''), (':', ';'), ('<', ','), ('>', '.'), ('?', '/'), ('{', '['), ('}', ']')].into_iter());
    let alphabet: HashSet<_> = String::from("abcdefghijklmnopqrstuvwxyz;,./'[]").chars().into_iter().collect();
    let alphabet = Alphabet::new(alphabet, &conversions);
    let parameters = {
        let base = Keyboard::new("qwert", "asdfg", "zxcv", ['a', 's', 'd', 'f'],
                                  "yuiop[]", "hjkl;'", "bnm,./", [';', 'l', 'k', 'j'],
                                  STAGGERED_STAGGER, STAGGERED_STAGGER);
        let mut texts_dir = PathBuf::new();
        texts_dir.push("samples");
        assert!(texts_dir.is_dir());
        let sample_size = 3;
        let brood_size = 16;
        let mutation_rate = 0.1;
        eco_parameters::EcoParameters::new(base, texts_dir, sample_size, brood_size, mutation_rate)
    };
    let mut rng = rand::thread_rng();
    let mut genomes = Vec::with_capacity(128);
    for _ in 0..genomes.capacity() {
        let len = rng.gen_range(1, 32);
        let mut genome = Genome { genome: Vec::with_capacity(len) };
        for _ in 0..len {
            let swaps = alphabet.choose_multiple(&mut rng, 2);
            genome.genome.push(SwapPoint(swaps[0], swaps[1]));
        }
        genomes.push(genome);
    }
    Ecosystem::new(genomes, alphabet, parameters)
}

fn main() {
    let mut eco = init();
    let mut stable_generations = 0;
    let mut last_best = std::f32::INFINITY;
    let mut gen = 0;
    // while stable_generations < 100 {
    loop {
        println!("Generation: {}\nBest cost: {}\n{}", gen, eco.best_cost(), eco.best_solution());
        if last_best <= eco.best_cost() {
            stable_generations += 1;
        } else {
            stable_generations = 0;
            last_best = eco.best_cost();
        }
        eco.next_generation();
        gen += 1;

        if gen % 64 == 0 { eco.recalculate(); }
    }
    println!("Got a solution:\n{}", eco.best_solution());
}
